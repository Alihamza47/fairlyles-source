<?php
    require_once 'vendor/autoload.php';
    include "includes/header.php";

    // $client = new \Contentful\Delivery\Client('tFatCxcms02OnHVaxgGg1X53ybXeElMUj-CkfXv90ig', '1ljerv8zj7cv');
    // $entries = $client->getEntries();
    // $asset = $client->getAsset('1GTAK50mb7fUYP9wtG4s9f');

    // echo json_encode($asset);
?>
    <!-- ====================== Main Slider Starts Here ====================== -->
    <div class="container-fluid header-slider-container">
        <div class="slick-slider">
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/model-foures.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h3>Fourés</h3>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/model-geia-sou-2.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h3>Fourés</h3>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/ipad-web-ui-design.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h3>Fourés</h3>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/macbook-web-ui-design.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h3>Fourés</h3>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/browser-web-ui-design-1.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h3>Fourés</h3>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/browser-web-ui-design-0.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h3>Fourés</h3>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/double-billboard-design.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h3>Fourés</h3>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/browser-web-ui-design-2.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h3>Fourés</h3>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
        </div>
        <div class="slides-numbers" style="display: block;">
            <span class="active">01</span> / <span class="total"></span>
        </div>
    </div>
    <!-- ====================== Main Slider Ends Here ====================== -->

    <!-- ====================== Blog new starts Here ====================== -->
    <div class="container-fluid latest-blog-container">
        <div class="d-flex justify-content-between">
            <h2 class="blog-header-title">
                Latest
            </h2>
            <a href="/insight" class="latest-blog-link">
                <svg xmlns="http://www.w3.org/2000/svg" height="36px" viewBox="0 0 24 24" width="36px" fill="#000000">
                    <path d="M0 0h24v24H0V0z" fill="none" />
                    <path d="M10.02 6L8.61 7.41 13.19 12l-4.58 4.59L10.02 18l6-6-6-6z" /></svg>
            </a>
        </div>
        <div class="row mt-60" id="index-slider">
            <div class='col-md-6 blog-slide'>
                <img src='assets/images/nfts.jpg' class='blog-cover-img' alt=''>
                <a href="blog-detail.php" class='blog-text-header'>12 Jan . Branding</a>
                <a href='blog-detail.php' class='blog-link'>
                    <p class='blog-text-para'>How Tesla is Leading The Charge When it Comes to Sustainability</p>
                </a>
            </div>
            <div class='col-md-6 blog-slide'>
                <img src='assets/images/airbnb.jpg' class='blog-cover-img' alt=''>
                <a href="blog-detail.php" class='blog-text-header'>12 Jan . Branding</a>
                <a href='blog-detail.php' class='blog-link'>
                    <p class='blog-text-para'>Airbnb is Changing How We See Accomodation in 2022</p>
                </a>
            </div>
            <div class='col-md-6 blog-slide'>
                <img src='assets/images/airbnb.jpg' class='blog-cover-img' alt=''>
                <a href="blog-detail.php" class='blog-text-header'>12 Jan . Branding</a>
                <a href='blog-detail.php' class='blog-link'>
                    <p class='blog-text-para'>Airbnb is Changing How We See Accomodation in 2022</p>
                </a>
            </div>
        </div>
    </div>
<?php include "includes/footer.php";?>