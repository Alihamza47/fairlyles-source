<?php 
    use Prismic\Api;
    use Prismic\LinkResolver;
    use Prismic\Predicates;
    use Prismic\Dom\RichText;
    $api = Api::get('https://gamination.prismic.io/api/v2');
    include "header.php";
    $getid = $_GET['id'];
    $response = $api->query(
        Predicates::at('document.id', $getid)
    );
    // echo json_encode($response);
    $results = $response->results;
?>
<!-- ====================== Main Header Starts Here ====================== -->
<div class="container insight-second-container">
    <div class="d-flex justify-content-between align-items-center">
        <h3>Project Information</h3>
        <span class="negative"></span>
    </div>
    <div class="mt-60">
        <div class="row">
            <div class="col-md-6">
                <h4 class="project-name">
                    <?= RichText::asText($results[0]->data->project_name) ?>
                </h4>
                <p class="project-slogan">
                    <?= RichText::asText($results[0]->data->project_slogan) ?>
                </p>
            </div>
            <div class="col-md-6">
                <p class="project-desc">
                    <?= RichText::asText($results[0]->data->project_disc) ?>
                </p>
            </div>
        </div>
        <?php
            $data = [];
            foreach ($results[0]->data as $key => $result) {
                if (property_exists($result, 'url')){
                    array_push($data, $result->url);
                }
            }
        ?>
        <!-- nth-child(3n+1) -->
        <div class="row" id="galary-section">
            <script>
                var data = <?php echo json_encode($data) ;?>;
                $.each(data, function( index, value ) {
                    if(index % 3 == 0){
                        size = 12;
                    }else{
                        size = 6;
                    }
                    var element = `
                        <div class="col-md-${size} mt-60">
                            <img src="${value}" class="work-image-col" alt="">
                        </div>
                    `;
                    $("#galary-section").append(element);

                });
            </script>
        </div>
    </div>
</div>

<?php include "footer.php"?>