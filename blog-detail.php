<?php 
    use Prismic\Api;
    use Prismic\LinkResolver;
    use Prismic\Predicates;
    use Prismic\Dom\RichText;
    $api = Api::get('https://gamination.prismic.io/api/v2');
    include "header.php";
    $getId = $_GET['id'];
    $response = $api->query(
        Predicates::at('document.id', $getId)
    );
    $data = $response->results;
?>
<!-- ====================== Main Header Starts Here ====================== -->
<div class="container insight-second-container p-0">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h1 class="blog_title">
                <?php echo  $data[0]->data->blog_title[0]->text;?>
            </h1>
            <div class="blog-info d-flex justify-content-between">
                <div class="">
                    <p class="blog-meta">
                        By fairlyles
                        <span class="d-block text-secondary">
                            <?php echo $date = date("d M", strtotime($data[0]->last_publication_date));?> . 
                            <?php 
                                foreach($data[0]->tags as $tags){
                                    echo $tags . ' ';
                                }
                            ?>
                        </span>
                    </p>
                </div>
                <div class="mt-2">
                    <a href="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M4.98 3.5c0 1.381-1.11 2.5-2.48 2.5s-2.48-1.119-2.48-2.5c0-1.38 1.11-2.5 2.48-2.5s2.48 1.12 2.48 2.5zm.02 4.5h-5v16h5v-16zm7.982 0h-4.968v16h4.969v-8.399c0-4.67 6.029-5.052 6.029 0v8.399h4.988v-10.131c0-7.88-8.922-7.593-11.018-3.714v-2.155z"/></svg>
                    </a>
                    <a href="">
                        <img src="assets/images/share.png" class='blog-icon' alt="">
                    </a>
                </div>    
            </div>
            <img src="<?php echo  $data[0]->data->blog_image->url;?>" class="img-fluid mt-60" alt="<?php echo  $data[0]->data->blog_image->alt;?>">
            <p class="blog-detail mt-60">
                <?php echo  $data[0]->data->blog_description[0]->text;?>
            </p>

            <div class="container blog-container">
                <h3 class="blog-latter-heading">
                    Get the latest in branding delivered to your inbox
                </h3>
                <div class="row mt-60">
                    <div class="position-relative">
                        <input type="text" class="newslatter-input-sub">
                        <button class="newslatter-submit">
                            <svg xmlns="http://www.w3.org/2000/svg" height="36px" viewBox="0 0 24 24" width="36px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none" /><path d="M10.02 6L8.61 7.41 13.19 12l-4.58 4.59L10.02 18l6-6-6-6z" /></svg>
                        </button>
                    </div>
                    <div class="subs-container mt-60">
                        <p>Unsubscribe at any time, no hard feelings.</p>
                        <a href="">Privacy Policy</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
        
    </div>
</div>
<div class="container blog-detail-container">
    <div class="row" id="insight-slider">
        <?php
            $getBlogs = $api->query(
                Predicates::at('document.type', 'blog-template'),
                [ 'orderings' => '[my.blog-template.date desc]' ]
            );
            $results = $getBlogs->results;
            $i = 0;
            foreach ($results as $result) {
                $id = $result->id;
                $title = $result->data->blog_title[0]->text;
                $image = $result->data->blog_image->url;
                $blogDate = explode("T", $result->first_publication_date);
                $pub_date = date("d M", strtotime($blogDate[0]));
                $tags = $result->tags[0];
                echo "<div class='col-md-6 pe-3'>
                        <img src='$image' class='blog-cover-img' alt=''>
                        <h1 class='blog-text-header'>$pub_date . $tags</h1>
                        <a href='blog-detail?id=$id' class='blog-link'>
                            <p class='blog-text-para'>$title</p>
                        </a>
                    </div>";
                $i++;
                if($i==2) break;
            }
        ?>
    </div>
</div>

<?php include "footer.php"?>