const swup = new Swup();

function init() {
    var helpers = {
        addZeros: function (n) {
            return (n < 10) ? '0' + n : '' + n;
        }
    };

    function sliderInit() {
        var $slider = $('.slick-slider');
        $slider.each(function () {
            var $sliderParent = $(this).parent();
            $(this).slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                prevArrow: false,
                nextArrow: false,
                infinite: true,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        adaptiveHeight: true
                    }
                }]
            });
            if ($(this).find('.item').length > 1) {
                $(this).siblings('.slides-numbers').show();
            }
            $(this).on('afterChange', function (event, slick, currentSlide) {
                $sliderParent.find('.slides-numbers .active').html(helpers.addZeros(currentSlide + 1));
            });
            var sliderItemsNum = $(this).find('.slick-slide').not('.slick-cloned').length;
            $sliderParent.find('.slides-numbers .total').html(helpers.addZeros(sliderItemsNum));
        });
    };

    sliderInit();

    // sliding the navbar
    var href = document.location.href;
    var lastPathSegment = href.substr(href.lastIndexOf('/') + 1);
    $(".icon").on("click", function (e) {
        if ($(".header-menu-links").is(":visible")) {
            $(".header-menu-links").fadeOut();
            if (lastPathSegment == "/about.php") {
                document.documentElement.style.setProperty('--navbar-toggler', "#fff");
            } else {
                document.documentElement.style.setProperty('--navbar-toggler', "#000");
            }
        } else {
            $(".header-menu-links").fadeIn();
            document.documentElement.style.setProperty('--navbar-toggler', "#fff");

        }
    })
    $(".play-btn").on("click", function (e) {
        e.preventDefault();
        $('#video-about-us').get(0).play();
        $('#video-about-us').attr("controls");
    })
    function changeColors() {
        document.documentElement.style.setProperty('--body-color', "#000");
        document.documentElement.style.setProperty('--fill-color', "#fff");
        document.documentElement.style.setProperty('--navbar-toggler', "#fff");
        document.documentElement.style.setProperty('--black-color', "#fff");
    }
    if (lastPathSegment == "about.php") {
        document.documentElement.style.setProperty('--body-color', "#000");
        document.documentElement.style.setProperty('--fill-color', "#fff");
        document.documentElement.style.setProperty('--navbar-toggler', "#fff");
        document.documentElement.style.setProperty('--black-color', "#fff");
    }else{
        document.documentElement.style.setProperty('--body-color', "#fff");
        document.documentElement.style.setProperty('--fill-color', "#000");
        document.documentElement.style.setProperty('--navbar-toggler', "#000");
        document.documentElement.style.setProperty('--black-color', "#000");
    }



    // Slick Slider for insight page
    $('#insight-slider').slick({
        slidesToShow: 2,
        slidesToScroll: 2,
        dots: false,
        arrows: true,
        infinite: true,
        prevArrow: $('#prev-slide'),
        nextArrow: $('#next-slide'),
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $("#index-slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: false,
        prevArrow: false,
        nextArrow: false,
        infinite: true,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    adaptiveHeight: true
                }
            }
        ]
    });
    var window_width = $(window).width();
    if(window_width < 775){
        // hide the topbutton on page load/ready.
        $('.back-to-top').hide();
    
        //Check to see if the window is top if not then display button
        $(window).scroll(function(){
            if ($(this).scrollTop() > 900) {
                $('.back-to-top').show().fadeIn();
            } else {
                $('.back-to-top').fadeOut().hide();
            }
        });
        //Click event to scroll to top
        $('.back-to-top').click(function(){
            $('html, body').animate({scrollTop : 0},360);
            return false;
        });
    }else{
        $('.back-to-top').hide();
    }
    $(".open-close").on("click", function (e) {
        e.preventDefault();
        var ul = $(this).parent().next('ul');
        ul.slideToggle();   
    })
    // $("#index-slider").closest('div').find('.slick-list').css('padding',"0 20% 0 0 !important");

}
init();
swup.on('contentReplaced', init);