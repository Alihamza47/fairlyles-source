
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fairlyles</title>
    <link rel="stylesheet" href="assets/fonts/font/stylesheet.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">

    <!-- ====================== Slick Slider CSS ====================== -->
    <link rel="stylesheet" href="assets/slick-slider/slick.css">
    <link rel="stylesheet" href="https://use.typekit.net/yco6qpt.css">
    <script src="assets/js/jquery.js"></script>

    <style>
        .transition-fade {
            transition: 0.4s;
            opacity: 1;
        }

        html.is-animating .transition-fade {
            opacity: 0;
        }
    </style>
</head>

<body>
    <main id="swup" class="transition-fade">
    <!-- ====================== Main Navigation starts here ====================== -->
    <div class="container-fluid p-0">
        <div class="main-navbar">
            <a href="index.php">
                <svg height="auto" viewBox="0 0 600 126" width="120px" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="m89.56 27.455v-17.842h-89.531v90.5h20.233v-33.873h58.423v-17.84h-58.423v-20.945h69.3zm38.43-1.293c-12.392 0-26.05 3.1-35.155 7.886l5.817 13.187c8.347-4.654 15.934-5.947 25.418-5.947 16.693 0 25.165 5.689 25.165 18.876v4.654c-5.311-6.076-15.554-11.118-28.958-11.118-14.922 0-34.27 5.947-34.27 24.435s19.348 24.564 34.27 24.564c13.4 0 23.647-5.171 28.958-11.118v.129a17.919 17.919 0 0 0 2.529 8.4h20.107a26.858 26.858 0 0 1 -3.161-13.575v-26.242c0-22.108-15.302-34.131-40.72-34.131zm21.625 52.878c-5.185 8.4-15.555 10.6-24.533 10.6-8.093 0-19.98-2.069-19.98-11.507s11.887-11.377 19.98-11.377c8.978 0 19.348 2.069 24.533 10.472v1.81zm43.615-58.7c6.828 0 11.634-4.267 11.634-10.084 0-6.076-4.806-10.214-11.634-10.214-6.956 0-11.887 4.137-11.887 10.214 0 5.822 4.931 10.088 11.887 10.088zm9.99 8.533h-19.6v71.236h19.6v-71.232zm34.517 71.236v-36.971c4.173-12.538 15.933-18.618 31.108-18.618v-17.2c-13.4 0-25.418 5.559-31.108 16.29v-14.733h-19.6v71.236h19.6zm58.161 0v-95.667h-19.598v95.671h19.6zm75.742-71.232-24.027 48.223-24.786-48.223h-20.612l35.408 66.841-16.566 30.252h19.98l50.836-97.093zm46.145 71.236v-95.671h-19.6v95.671zm29.715-31.028h66.769a14.146 14.146 0 0 0 .506-4.654c0-25.211-16.945-38.4-42.237-38.4-27.441 0-43.88 14.738-43.88 38.527 0 23.659 16.566 38.4 45.271 38.4 16.06 0 28.58-3.879 38.317-12.67l-10.5-11.765c-7.208 5.947-14.8 9.179-26.809 9.179-16.186-.002-25.671-5.818-27.437-18.617zm25.165-27.8c12.393 0 20.359 4.525 23.394 14.739l-47.674.259c2.778-10.083 11.504-14.995 24.276-14.995zm89.388 14.225c-13.152-.776-22.636-1.681-22.636-7.111 0-5.818 11.508-7.111 19.6-7.111 12.519 0 21.751 3.232 28.832 8.016l8.979-12.67c-8.6-5.818-22.13-10.6-36.8-10.6-26.3 0-38.949 9.826-38.949 23.659 0 15.9 18.716 20.815 38.316 22.237 14.8 1.034 22.51 2.974 22.51 8.145 0 5.559-8.473 7.628-19.6 7.628-15.048 0-27.188-4.4-34.4-10.731l-9.485 11.894c9.358 8.145 25.292 14.093 43.375 14.093 25.418 0 38.19-10.6 38.19-24.306.005-15.253-16.055-21.853-37.932-23.143z"
                        fill-rule="evenodd" /></svg>
            </a>
            <input type="checkbox" id="menu">

            <label for="menu" class="icon">
                <div class="menu"></div>
            </label>
        </div>
    </div>
    <!-- ====================== Main Navigation ends here ====================== -->
    <div class="header-menu-links">
        <ul>
            <li><a href="./work.php">Work</a></li>
            <li><a href="./about.php">About</a></li>
            <li><a href="./insight.php">Insights</a></li>
            <li><a href="./contact.php">Contact</a></li>
        </ul>
        <div class="row header-menu-links-contact">
            <div class="col-6 col-md-3">
                <h1>work@fairlyles.com</h1>
                <h1 class="mt-3">+44 20 3920 7472</h1>
            </div>
            <div class="col-5 col-md-4">
                <a href="" class="header-menu-links-socail-media">Instagram</a>
                <a href="" class="header-menu-links-socail-media">LinkedIn</a>
            </div>
        </div>
    </div>