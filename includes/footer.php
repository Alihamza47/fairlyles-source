    <!-- ====================== Footer starts here ====================== -->
    <div class="container-fluid footer-section">
        <div class="d-flex align-items-center justify-content-between">
            <div class="d-flex align-items-center">
                <h1 class="copy-right-section">
                    &copy; 2022 Fairlyles
                </h1>
                <a href="" class="footer-links">
                    Policy
                </a>
            </div>
            <div class="d-none d-lg-block">
                <div class="d-flex">
                    <a href="" class="footer-links">
                        Instagram
                    </a>
                    <a href="" class="footer-links">
                        Twitter
                    </a>
                    <a href="" class="footer-links">
                        LinkedIn
                    </a>
                </div>
            </div>
        </div>
    </div>
        <button class="back-to-top">
            <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M4 12l1.41 1.41L11 7.83V20h2V7.83l5.58 5.59L20 12l-8-8-8 8z"/></svg>
        </button>
    </main>



    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- ====================== Slick Slider JS ====================== -->
    <script src="assets/slick-slider/slick.min.js"></script>

    <!-- ====================== Custom JS ====================== -->
    <script src="https://unpkg.com/swup@latest/dist/swup.min.js"></script>
    <script src="assets/js/custom.js"></script>
    </body>

    </html>