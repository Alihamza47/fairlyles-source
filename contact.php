<?php include "includes/header.php"?>
<!-- ====================== Main Header Starts Here ====================== -->
<div class="container-fluid contact-header-container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="header-heading">
                Lets Chat
            </h1>
        </div>
    </div>
    <div class="row contact-container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="contact-info">
                    New Business <br>
                    <span>
                        work@fairlyles.com
                    </span>
                </h1>
                <h1 class="contact-info mt-60">
                    Careers <br>
                    <span>
                        careers@fairlyles.com
                    </span>
                </h1>
                <h1 class="contact-info mt-60">
                    General Inquiries <br>
                    <span>
                        hello@fairlyles.com
                    </span>
                </h1>
            </div>
            <div class="col-md-6 ">
                <h1 class="contact-info">
                    London
                    <br><br>
                    85 Great Portland Street <br>
                    First Floor, London <br>
                    W1W 7LT
                    <br><br>
                    +44 20 3920 7472
                </h1>
                <h1 class="contact-info mt-60">
                    Socials
                </h1>
                <a href="" class="contact-social-links">Instagram</a>
                <a href="" class="contact-social-links">Twitter</a>
                <a href="" class="contact-social-links">LinkedIn</a>
            </div>
        </div>
    </div>
</div>

<?php include "includes/footer.php"?>