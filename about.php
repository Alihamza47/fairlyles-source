<?php include "includes/header.php"?>

<!-- ====================== Main Header Starts Here ====================== -->
<div class="container-fluid aboutus-header-container">
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-lg-6 col-md-12">
            <p class="header-heading">
                Fairlyles is a strategic brand & design agency. We help businesses grow, scale and be more than just
                storefronts for products and services. Building brands with purpose: to navigate disruption, shape a
                better future and change people’s lives.
            </p>
        </div>
    </div>
</div>
<!-- ====================== Main Header ends Here ====================== -->

<!-- ====================== Second section Starts Here ====================== -->
<div class="container-fluid about-us-intro">
    <div class="about-us-video">
        <video width="100%" height="auto" poster="./assets//images/showreel_placeholder.jpg" id="video-about-us">
            <source src="./assets/images/showreel_preview.mp4" type="video/mp4">
            Your browser does not support the video tag.
        </video>
        <a href="" class="play-btn">
            <svg xmlns="http://www.w3.org/2000/svg" height="48px" viewBox="0 0 24 24" width="48px">
                <path d="M0 0h24v24H0z" fill="none" />
                <path d="M8 5v14l11-7z" /></svg>
        </a>
    </div>
</div>
<!-- ====================== Second section ends Here ====================== -->

<!-- ====================== third section Starts Here ====================== -->

<div class="container-fluid aboutus-third-section-container">
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-lg-6 col-md-12">
            <p class="aboutus-section-heading">
                Growing businesses reach a crossroads. They want to
                keep things moving, but their exisiting brand has ran
                out of steam. We help them rediscover who they are
                re-tell their story—using empathy, strategy and
                imagination.
            </p>
        </div>
    </div>
</div>
<!-- ====================== Second section ends Here ====================== -->

<!-- ====================== Fourth section ends Here ====================== -->
<div class="container-fluid about-us-forth-section">
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <h1 class="aboutus-section-heading">What we do</h1>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="d-flex justify-content-between align-items-end">
                        <h3 class="aboutus-section-heading mt-m-60">Strategy</h3>
                        <button class="open-close">
                            <img src="assets/images/close_light.png" alt="">
                        </button>
                    </div>
                    <ul class="mt-m-60">
                        <li>Insights & Analysis</li>
                        <li>Journey Mapping</li>
                        <li>Brand Strategy</li>
                        <li>Brand Positioning</li>
                        <li>Brand Communication</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="d-flex justify-content-between align-items-end">
                        <h1 class="aboutus-section-heading mt-m-60">Identity</h1>
                        <button class="open-close">
                            <img src="assets/images/close_light.png" alt="">
                        </button>
                    </div>
                    <ul class="mt-m-60">
                        <li>Naming</li>
                        <li>Visual & Verbal Identity</li>
                        <li>Tone of Voice</li>
                        <li>Creative Direction</li>
                        <li>Brand Architecture</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-12 mt-about-skills-60">
                    <div class="d-flex justify-content-between align-items-end">
                        <h1 class="aboutus-section-heading mt-m-60">Collateral</h1>
                        <button class="open-close">
                            <img src="assets/images/close_light.png" alt="">
                        </button>
                    </div>
                    <ul class="mt-m-60">
                        <li>UI & UX Design</li>
                        <li>Front-End Design</li>
                        <li>Print & Packaging</li>
                        <li>Signage & Wayfinding</li>
                        <li>Copywriting</li>
                        <li>Social Media</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid about-us-intro">
    <div class="about-us-video">
        <img src="assets/images/about.jpg" class="img-about-us-section" alt="">
    </div>
</div>

<div class="container-fluid about-us-forth-section">
    <div class="row">
        <div class="col-md-12 col-lg-6">
            <h3 class="aboutus-section-heading">Awards & Recognition</h3>
        </div>
        <div class="col-md-12 col-lg-6 mt-m-60">
            <div class="row">
                <div class="col-6 col-md-6">
                    <h3 class="aboutus-section-heading">Adobe Behance</h3>
                    <h3 class="aboutus-section-heading color-secondary">InDesign</h3>
                </div>
                <div class="col-6 col-md-6 text-end">
                    <h3 class="aboutus-section-heading color-secondary">2022</h3>
                </div>
            </div>
            <div class="row mt-60">
                <div class="col-6 col-md-6">
                    <h3 class="aboutus-section-heading">Mindsparkle Mag</h3>
                    <h3 class="aboutus-section-heading color-secondary">InDesign</h3>
                </div>
                <div class="col-6 col-md-6 text-end">
                    <h3 class="aboutus-section-heading color-secondary">2021</h3>
                </div>
            </div>
            <div class="row mt-60">
                <div class="col-6 col-md-6">
                    <h3 class="aboutus-section-heading">Lovably</h3>
                    <h3 class="aboutus-section-heading color-secondary">InDesign</h3>
                </div>
                <div class="col-6 col-md-6 text-end">
                    <h3 class="aboutus-section-heading color-secondary">2021</h3>
                </div>
            </div>
            <div class="row mt-60">
                <div class="col-6 col-md-6">
                    <h3 class="aboutus-section-heading">Muzli</h3>
                    <h3 class="aboutus-section-heading color-secondary">InDesign</h3>
                </div>
                <div class="col-6 col-md-6 text-end">
                    <h3 class="aboutus-section-heading color-secondary">2021</h3>
                </div>
            </div>
            <div class="row mt-60">
                <div class="col-6 col-md-6">
                    <h3 class="aboutus-section-heading">Adobe Behance</h3>
                </div>
                <div class="col-6 col-md-6 text-end">
                    <h3 class="aboutus-section-heading color-secondary">2021</h3>
                </div>
                <div class="col-6 col-md-6">
                    <h3 class="aboutus-section-heading color-secondary">Branding</h3>
                </div>
            </div>
            <div class="row mt-60">
                <div class="col-6 col-md-6">
                    <h3 class="aboutus-section-heading">Adobe Behance</h3>
                    <h3 class="aboutus-section-heading color-secondary">Branding</h3>
                </div>
                <div class="col-6 col-md-6 text-end">
                    <h3 class="aboutus-section-heading color-secondary">2020</h3>
                </div>
            </div>
            <div class="row mt-60">
                <div class="col-6 col-md-6">
                    <h3 class="aboutus-section-heading">Klikkenthéke</h3>
                    <h3 class="aboutus-section-heading color-secondary">InDesign</h3>
                </div>
                <div class="col-6 col-md-6 text-end">
                    <h3 class="aboutus-section-heading color-secondary">2019</h3>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include "includes/footer.php"?>