<?php
    require_once 'vendor/autoload.php';
    include "includes/header.php";

    // $client = new \Contentful\Delivery\Client('tFatCxcms02OnHVaxgGg1X53ybXeElMUj-CkfXv90ig', '1ljerv8zj7cv');
    // $entries = $client->getEntries();
    // $asset = $client->getAsset('1GTAK50mb7fUYP9wtG4s9f');

    // echo json_encode($asset);
?>
    <!-- ====================== Main Slider Starts Here ====================== -->
    <div class="container header-slider-container">
        <div class="slick-slider">
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/model-foures.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h1>Fourés</h1>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/model-geia-sou-2.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h1>Fourés</h1>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/ipad-web-ui-design.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h1>Fourés</h1>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/macbook-web-ui-design.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h1>Fourés</h1>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/browser-web-ui-design-1.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h1>Fourés</h1>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/browser-web-ui-design-0.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h1>Fourés</h1>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/double-billboard-design.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h1>Fourés</h1>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
            <div class="main-slide">
                <div class="slider-image">
                    <img src="assets/images/browser-web-ui-design-2.png" alt="">
                </div>
                <div class="slider-content-container">
                    <h1>Fourés</h1>
                    <p>Parisian fashion meets sustainability</p>
                </div>
            </div>
        </div>
        <div class="slides-numbers" style="display: block;">
            <span class="active">01</span> / <span class="total"></span>
        </div>
    </div>
    <!-- ====================== Main Slider Ends Here ====================== -->

    <!-- ====================== Blog new starts Here ====================== -->
    <div class="container latest-blog-container">
        <div class="d-flex justify-content-between">
            <h1 class="blog-header-title">
                Latest
            </h1>
            <a href="/insight" class="latest-blog-link">
                <svg xmlns="http://www.w3.org/2000/svg" height="36px" viewBox="0 0 24 24" width="36px" fill="#000000">
                    <path d="M0 0h24v24H0V0z" fill="none" />
                    <path d="M10.02 6L8.61 7.41 13.19 12l-4.58 4.59L10.02 18l6-6-6-6z" /></svg>
            </a>
        </div>
        <div class="row mt-60" id="index-slider">
            <div class='col-md-6 pe-3'>
                <img src='assets/images/nfts.jpg' class='blog-cover-img' alt=''>
                <h1 class='blog-text-header'>12 Jan . Branding</h1>
                <a href='blog-detail?id=$id' class='blog-link'>
                    <p class='blog-text-para'>How NFT's Are changing word</p>
                </a>
            </div>
            <div class='col-md-6 pe-3'>
                <img src='assets/images/nfts.jpg' class='blog-cover-img' alt=''>
                <h1 class='blog-text-header'>12 Jan . Branding</h1>
                <a href='blog-detail?id=$id' class='blog-link'>
                    <p class='blog-text-para'>How NFT's Are changing word</p>
                </a>
            </div>
        </div>
    </div>
<?php include "includes/footer.php";?>
<script>
    $("#index-slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: false,
        prevArrow: false,
        nextArrow: false,
        infinite: true,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    adaptiveHeight: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
</script>