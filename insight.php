<?php 
    include "includes/header.php";
?>
<!-- ====================== Main Header Starts Here ====================== -->
<div class="container-fluid header-container">
    <div class="row">
        <div class="col-md-7 col-insight-1">
            <img src="assets/images/nfts.jpg" class="insight-image-header-1" alt="">
            <a class="blog-text-header">31 Jan . Branding</a>
            <p class="insight-blog-para font-reckless">How NFTs Are Changing The Way 
                We See The Future
            </p>
        </div>
        <div class="col-md-5 d-none d-md-block col-insight-2">
            <img src="assets/images/oculus.jpg" class="insight-image-header-2" alt="">
            <a class="blog-text-header ">31 Jan . Branding</a>
            <p class="insight-blog-para">How Tesla is leading the charge when it comes to sustainability</p>
        </div>
    </div>
</div>
<div class="container-fluid insight-second-container">
    <div class="d-flex justify-content-between">
        <h3 class="news-latter-heading read-more">
            Read more
        </h3>
        <a href="" class="latest-blog-link" id='prev-slide'>
        <svg xmlns="http://www.w3.org/2000/svg" height="36px" viewBox="0 0 24 24" width="36px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12l4.58-4.59z"/></svg>
        </a>
        <a href="" class="latest-blog-link" id="next-slide">
            <svg xmlns="http://www.w3.org/2000/svg" height="36px" viewBox="0 0 24 24" width="36px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none" /><path d="M10.02 6L8.61 7.41 13.19 12l-4.58 4.59L10.02 18l6-6-6-6z" /></svg>
        </a>
    </div>
    <div class="row mt-60" id="insight-slider">
            <div class='col-md-6 blog-slide'>
                <img src='assets/images/nfts.jpg' class='blog-cover-img' alt=''>
                <a href="blog-detail.php" class='blog-text-header'>12 Jan . Branding</a>
                <a href='blog-detail.php' class='blog-link'>
                    <p class='blog-text-para'>How Tesla is Leading The Charge When it Comes to Sustainability</p>
                </a>
            </div>
            <div class='col-md-6 blog-slide'>
                <img src='assets/images/airbnb.jpg' class='blog-cover-img' alt=''>
                <a href="blog-detail.php" class='blog-text-header'>12 Jan . Branding</a>
                <a href='blog-detail.php' class='blog-link'>
                    <p class='blog-text-para'>Airbnb is Changing How We See Accomodation in 2022</p>
                </a>
            </div>
            <div class='col-md-6 blog-slide'>
                <img src='assets/images/airbnb.jpg' class='blog-cover-img' alt=''>
                <a href="blog-detail.php" class='blog-text-header'>12 Jan . Branding</a>
                <a href='blog-detail.php' class='blog-link'>
                    <p class='blog-text-para'>Airbnb is Changing How We See Accomodation in 2022</p>
                </a>
            </div>
        </div>
</div>
<div class="container-fluid insight-third-container">
    <div class="row mt-60">
        <div class="col-md-2"><span></span></div>
        <div class="col-md-8 mobile-column-subscribe">
            <div class="position-relative">
                <h3 class="news-latter-heading">
                    Get the latest in branding delivered to your inbox
                </h3>
                <div class="d-flex justify-content-center mt-60">
                    <div class="position-relative">
                        <input type="text" class="newslatter-input-sub">
                        <button class="newslatter-submit">
                            <svg xmlns="http://www.w3.org/2000/svg" height="36px" viewBox="0 0 24 24" width="36px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none" /><path d="M10.02 6L8.61 7.41 13.19 12l-4.58 4.59L10.02 18l6-6-6-6z" /></svg>
                        </button>
                    </div>
                </div>
            </div>
            <div class="subs-container">
                <p>Unsubscribe at any time, no hard feelings.</p>
                <a href="">Privacy Policy</a>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>

<?php include "includes/footer.php"?>